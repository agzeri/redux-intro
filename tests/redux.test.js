const Redux = require("redux");

xtest("Sprint 1 | Redux should be installed", () => {  
  expect(Redux).toBeDefined();
});

xtest("Sprint 1 | `createStore` should be defined", () => {
  expect(Redux.createStore).toBeDefined();
  expect(Redux.combineReducers).toBeDefined();
});
